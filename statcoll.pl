
#!/usr/bin/perl

use strict;
use Getopt::Std;
use File::Copy;
use File::Basename;
use POSIX;

my %options=();
getopts("ahso:f:",\%options);
my $prefix = "mystat";
$SIG{'TERM'} = 'inthandler';
$SIG{'INT'} = 'inthandler';
  
&printhelp() if defined $options{h};

my $outputdir = "statcoll_output";
my $varlogmsg = "/var/log/messages";
#All intervals are in seconds
if (defined $options{o})
{
  $outputdir = $options{o};
}

#my $cachecleanup = `sync; echo 3 > /proc/sys/vm/drop_caches`;

#Tool entry point
&check_for_multiple_instances();
&startCollection();

sub collectEventViewer()
{
  #my $lisabinfolder = "/opt/fgw_instance_running/lisa/bin/";
  #chdir($lisabinfolder) or die;
  my $cmd = `/opt/fgw_instance_running/lisa/bin/LisaEventViewer > ./$outputdir/EventViewerOutput.out`;
  my $systemStatsFolder = "/opt/SystemStats";
  chdir($systemStatsFolder);
}

sub check_for_multiple_instances()
{
	my $processname = 'statcoll.pl';
	my $pids= `ps -eaf | grep "$processname" | grep -v "grep" | wc -l`; 
	if($pids > 1)
	{
		print "Already another instance of statcoll found. Exiting!!\n";
		exit(0);
	}
	print "Started statcoll.pl\n";
}

sub startCollection()
{
  &testrights();
  
  #if you want to multiple all intervals by a factor change this value
  my $factor = 1;
  $factor = $options{f} if defined $options{f};
  #repeat intervals, edit this to fine tune as per your requirement
  my $NETSTATINTERVAL=1*$factor;
  my $INTERRUPTSINTERVAL=1*$factor;
  my $TOPINTERVAL=1*$factor;
  my $LSOFINTERVAL=5*$factor;
  my $MPSTATINTERVAL=1*$factor;
  my $PROFILEINTERVAL=5*$factor;
  my $SCTPINTERVAL=1*$factor;
  my $QUEUEINTERVAL=1*$factor;
  my $TUNNELINTERVAL=1*$factor;
  my $DSTATINTERVAL=1*$factor;

 # &archiveOldFiles();
  
  mkdir $outputdir if(!-d $outputdir);
  
  echo("V1.3",">","$outputdir/version.txt");
  
  print "Please stop the data collection by doing CTRL+C. If that is not feasible, please do 'kill $$'. Never do 'kill -9' please!\n";
  
  #Prepare the PIDs that will be monitored by top
  my @pidlist = &prepareTop();
  &preparequeue();
  &prepareTunnel();
  
  #Top and mpstat will be collected directly outside the while loop
  #Reason: The average values for the Cpu statistics gets averaged to the 
  #system startup time if a single output of top is done
  &startTopCollection(\@pidlist,$TOPINTERVAL);
  &startDstatCollection($DSTATINTERVAL);
  &startMpstatCollection($MPSTATINTERVAL);
  
  logmsg ("Collecting statistics data...\n");
  #Start the while loop for data collection
  my $collCount=0;
  while(1)
  {
    my $date = &gettime();
    chomp($date);
    $date = "TIMESTAMP:$date";
  
    &collectnetstat($date) if(&isrighttime($collCount,$NETSTATINTERVAL));
    &collectinterrupts($date) if(&isrighttime($collCount,$INTERRUPTSINTERVAL));
    &collectlsof($date) if(&isrighttime($collCount,$LSOFINTERVAL) && defined $options{a});
    &collectprofile($date) if(&isrighttime($collCount,$PROFILEINTERVAL));
    &collectsctp($date) if(&isrighttime($collCount,$SCTPINTERVAL));
    &collectqueue($date) if(&isrighttime($collCount,$QUEUEINTERVAL));
    &collecttunnel($date) if(&isrighttime($collCount,$TUNNELINTERVAL));
    
    $collCount++;
    sleep(1);
    #if(($collCount % 100) == 0)
    #{
    #  my $cachecleanup = `sync; echo 1 > /proc/sys/vm/drop_caches`;
    #  print "Cleared cache ...\n";
    #}
  }
}

sub archiveOldFiles
{
  my $date = &gettime();
  if(-e $outputdir)
  {
    logmsg ("Archiving existing '$outputdir' directory to $date.statcoll.tar.gz\r");
    `tar -cvf $date.statcoll.tar $outputdir/ > /dev/null 2>&1`;
    `gzip $date.statcoll.tar > /dev/null 2>&1` if !$?;
    `tar tvfz $date.statcoll.tar.gz > /dev/null 2>&1`;
    if ( !$? )
    {
      `rm -rf $outputdir > /dev/null 2>&1`; 
      logmsg ("Archived existing '$outputdir' directory to $date.statcoll.tar.gz              \n");
    }
    else
    {
      logmsg ("Moved existing '$outputdir' directory to $date.statcoll.$outputdir\n");
      `mv $outputdir $date.statcoll.$outputdir`;
    }
  }
}
sub collectnetstat()
{
  my $date = shift;
  #logmsg("Collecting netstat data for $date\n");
  dumpdata($date,"/proc/net/dev","$outputdir/netstat.txt");
}

sub collectqueue()
{
  my $date = shift;
  #logmsg("Collecting msg queue data for $date\n");
  
  opendir(DIR, "msg_q");
  my @files= readdir(DIR);
  closedir(DIR); 

  open(FF,">>$outputdir/msgq.txt");
  print FF "$date\n";
  foreach(@files)
  {
    next if ($_ eq ".");
    next if ($_ eq "..");
    open(II,"<msg_q/$_");
    my @data = <II>;
    chomp(@data);
    print FF "$_ ".$data[0]."\n";
    close(II);
  }
  close(FF);
}

sub collectsctp()
{
  my $date = shift;
  #logmsg("Collecting sctp data for $date\n");
  dumpdata($date,"/proc/net/sctp/snmp","$outputdir/sctp.txt");
}

sub collectinterrupts()
{
  my $date = shift;
  #logmsg("Collecting interrupts data for $date\n");
  dumpdata($date,"/proc/interrupts","$outputdir/interrupts.txt");
}

sub collectlsof()
{
  my $date = shift;
  #logmsg("Collecting lsof data for $date\n");
  echo("dummy dummy dummy dummy $date",">>","$outputdir/lsof.txt");
  system("lsof >> $outputdir/lsof.txt&");
}

sub collectprofile()
{
  my $date = shift;
  if (-e "/proc/profile")
  {
    #logmsg("Collecting profile data for $date\n");
    dumpdata($date,"/proc/profile","$outputdir/profile.txt");
  }
}

sub collecttunnel()
{
  my $date = shift;
  #logmsg("Collecting tunnel data for $date\n");
  my $deletedTunnels = grep("Deleted","$outputdir/ipsec.txt");
  my $newTunnels = grep("Updated","$outputdir/ipsec.txt");
  chomp($deletedTunnels);
  chomp($newTunnels);
  $deletedTunnels = floor($deletedTunnels / 4);
  $date =~ s/TIMESTAMP://;
  echo("$date,$deletedTunnels,$newTunnels",">>","$outputdir/ipsecstat.txt");
}

sub prepareTunnel()
{
  &copyAndKill("/usr/local/6bin/ip");
  echo("TIMESTAMP,Deleted,Created",">","$outputdir/ipsecstat.txt");
  system("./".$prefix."ip xfrm monitor 2>&1 > $outputdir/ipsec.txt&");
}
sub preparequeue()
{
  mkdir "msg_q" if(!-d "msg_q");
  `mount -t mqueue none msg_q > /dev/null 2>&1`;
  logmsg("Preparing the list of message queues\r");

  opendir(DIR, "msg_q");
  my @files= readdir(DIR);
  closedir(DIR); 
  open(MLIST, ">$outputdir/msgqlist.txt");
  foreach(@files)
  {
    next if ($_ eq ".");
    next if ($_ eq "..");
    print MLIST "$_\n";
  }
  close(MLIST);
  
  logmsg("                                       \r");
}

sub prepareTop()
{
  my @pidlist;
  my @allpids = `ps -eaf|awk '{print \$2}'| grep -v PID`; 
  chomp(@allpids); 
  
  my $cmd = "cp .toprc \$HOME/.".$prefix."toprc";
  `$cmd`;
  
  while(1)
  { 
    if(scalar(@allpids)<20)
    {
      push(@pidlist, join(",",@allpids)) if (scalar(@allpids) > 0); 
      last; 
    }
    my @singletop; 
    for(my $i=0;$i<20;$i++)
    {
      push(@singletop,shift(@allpids)); 
    }
    push(@pidlist, join(",",@singletop)) if (scalar(@singletop) > 0);
  }
  return @pidlist;
}

sub copyAndKill()
{
  my $comm = shift;

  my $location = `which $comm`;
  chomp($location);  
  #logmsg ("$location\n");
  $comm = basename($comm);

  `kill \`ps -eaf | grep $prefix$comm | awk '{print \$2}'\``;

  #logmsg ("$prefix$comm\n");
  
  copy($location,"$prefix$comm");
  `chmod +x $prefix$comm`;
}

sub deleteAndKill()
{
  my $comm = shift;    
  logmsg("Stopping $comm\n");
  #logmsg("$prefix$comm\n");
  
  system("kill `ps -eaf | grep $prefix$comm | awk '{print \$2}'` > /dev/null 2>&1");
  unlink("$prefix$comm");
}

sub startTopCollection()
{
  my $pidlist = shift;
  my $collInterval = shift;
  #logmsg("Collecting top data with interval [$collInterval] seconds\n");
  &copyAndKill("top");
  system("./".$prefix."top -b -d $collInterval 2>&1> $outputdir/topoutput_0.txt&"); 
}

sub startDstatCollection()
{
  my $collInterval = shift;
  &copyAndKill("dstat");
  logmsg("Collecting dstat data ... \n");
  system("./".$prefix."dstat -tcdngy $collInterval 2>&1> $outputdir/dstatoutput_0.txt&");
}
sub startMpstatCollection()
{
  my $collInterval = shift;
  #logmsg("Collecting mpstat data with interval [$collInterval] seconds\n");
  &copyAndKill("mpstat");
  system("./".$prefix."mpstat -P ALL $collInterval 2>&1> $outputdir/mpstat.txt&");
}

sub gettime()
{
	my ($sec,$min,$hour,$day,$month,$yr19,@rest) =   localtime(time);
	return "$day-".++$month."-".($yr19+1900)."_".sprintf("%02d",$hour)."_".sprintf("%02d",$min)."_".sprintf("%02d",$sec);
}

sub isrighttime()
{
  my $collcount = shift;
  my $interval = shift;
  my $ret = 0;
  if($collcount eq 0)
  {
    $ret = 1;
  }
  elsif ( ($collcount >= $interval) and  (0 == $collcount % $interval) )
  {
    $ret = 1;
  }
  return $ret;
}

sub logmsg
{
  my $msg = shift;
  print $msg if not defined $options{s};
}

sub printhelp
{  
  print STDERR <<EOF;
  usage: $0 [ahs] [-f factor] [-o output]
     -a        : all statistics (includes lsof)
     -h        : this (help) message
     -s        : silent operation
     -f factor : collection interval factor
     -o output : output directory to store the data
EOF
  exit(0);
}

sub inthandler()
{	
  logmsg("Stopping data collection. Please wait...\n");
  `cp $varlogmsg $outputdir/`;
  &deleteAndKill("top");
  &deleteAndKill("mpstat"); 
  &deleteAndKill("ip");
  &deleteAndKill("dstat");
  `umount msg_q > /dev/null 2>&1`;
  rmdir("msg_q") if (-e "msg_q");
  
  unlink ($ENV{'HOME'}."/.".$prefix."toprc");
  
  logmsg("Stopped data collection.\n");
  &collectEventViewer();
  logmsg("Collected EventViewer Data in /opt/SystemStats/output/EventViewerOutput.out.\n");
  
  exit(0);
}
sub echo
{
  my $msg = shift;
  my $mode = shift;
  my $file = shift;
#  logmsg("msg [$msg],mode [$mode], file=[$file]\n");
  open (REPFILE, "$mode$file");
  print REPFILE "$msg\n";
  close REPFILE; 
}

sub grep
{
  my $text = shift;
  my $file = shift;
  open (REPFILE,'<$file');
  my @data = <REPFILE>;
  grep($text,@data);
  return scalar(@data);  
}

sub dumpdata
{
  my $date = shift;
  my $input = shift;
  my $output = shift;
  open(FF,">>$output");
  print FF "$date\n";
  open(II,"<$input");
  print FF <II>;
  close(FF);
  close(II);
}

sub testrights
{
  if( getlogin() ne 'root')
  {
    print "Please run the tool as 'root' user. Exiting.\n";
#    exit(1);
  }
  unlink("mytmpfile") if(-f "mytmpfile");
  `touch mytmpfile`;
  if($?)
  {
    unlink("mytmpfile") if(-f "mytmpfile");   
    print "The current user does not have permission to create files in this directory. Exiting.\n";
    exit(2);
  }
  unlink("mytmpfile") if(-f "mytmpfile");
}
