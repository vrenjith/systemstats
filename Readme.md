## Usage
### Before the test run
Copy this entire directory to a folder in the test machine.
Start the data collection using the command 'perl statcoll.pl&'
Note: The data is collected  under the directory 'output'.

### After the run
* Stop the data collection by stopping the previous perl command. (Run fg and then CTRL+c)
* Run the command 'perl analysedata.pl' which will generate all the analysed files under 'reports' sub directory.
* Transfer this entire sub directory 'reports' to a windows/mac machine which has Excel in it. Open the macros.xls that will be present in the copied directory and press the 'Generate Reports' button. (Don't forget to allow macros in Excel). This will generate a finalreport.xls containing various graphs for the test run. 

### CHANGELOG
### V1.3 - 01-Oct-2009
#### Features Added
* Multiple data collection (with different intervals) possible with different output directories
* Checks the file access rights before starting data collection
* Linux utilities changed to perl functions, this avoids creation of new processes for data collection (observed during CSW tests)
* Usage of netstat changed to /proc/net/dev for more accurate statistics of different interfaces
* Network packets and bytes plotted in seperate charts
* Resident Memory and CPU plotted in seperate charts
* ATM interrupt statistics added
#### Bugs corrected
* Handle m,g values from the resident memory data from top
### V1.2 - 21-Aug-2009
#### Features Added
* Added Ipsec tunnel creation/deletion statistics
* Added posix message queue statistics (all the queues present in the system) (with a mapping to process name)
* After analysis an archive of the collected data is made for storage
* Added command line options to avoid editing of scripts
> -f to give a factor to be used for collection interval   
> -a to collect every statistics (by default it will skip LSOF due to disk space requirements)   
> -h for help (obvious)
* Added collection and report for sctp statistics
* Added option -s to statcoll.pl for silent operation
#### Bugs corrected
* Timestamp sorting was wrong
### V1.1 - 14-Aug-2009
#### Features Added
* A single xls is generated at the end with all the graphs (which can be attached to the report mail)
* Process CPU/Mem chart also added to the final report
* Added mpstat interval variable to the collection script
* The first interval bug corrected.
* /proc/profile statistics collection added
### V1.1 - 12-Aug-2009
* First release
