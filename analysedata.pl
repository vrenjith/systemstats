#!/usr/local/bin/perl

use strict;
use File::Copy;
use File::Basename;
use Term::ANSIColor;
use Getopt::Std;
my %options=();
getopts("ho:r:",\%options);

&printhelp() if defined $options{h};

my $outputdir = "output";
my $reportsdir = "reports";
if (defined $options{o})
{
  $outputdir = $options{o};
}
if (defined $options{r})
{
  $reportsdir = $options{r};
}
if(!-d "$outputdir")
{
  print "No data found to analyse. The collected files are expected to be available in a directory named '$outputdir'\n";
  exit(0);
}

&archiveOldFiles();
if(!-e $reportsdir)
{
  if(!mkdir($reportsdir))
  {
    print color("red"),"Unable to create directory '$reportsdir'. Exiting...\n";
    print color("reset");
    exit(0);
  } 
}
`echo V1.3 > $reportsdir/version.txt`;

my $prefix = "mystat";

&extractFgwTop();
&extractDstat();
&extractTop();
&extractInterrupts();
&extractMpstat();
&extractNetstat();
&extractLsof();
&extractSctp();
&extractIpsec();
&extractMsgq();
&extractLisaEventViewer();

print "Generating IKE statistics files.....\nPlease wait it may take few minutes\n";
&ike_message_parser("$outputdir/messages");

copy("macros.xls","$reportsdir/macros.xls");
my $date = &gettime();
my $tarfile = "$date.statcoll.tar";
my $gzfile = "$tarfile.gz";

print "Compressing and storing collected data as [$gzfile]...\n";
`tar -cvf $tarfile $outputdir/ > /dev/null 2>&1`;
`gzip $tarfile > /dev/null 2>&1` if !$?;
`tar tvfz $gzfile > /dev/null 2>&1`;
if ( !$? )
{
  `rm -rf $outputdir > /dev/null 2>&1`; 
  print "Archiving complete. '$outputdir' directory removed.\n";
}
else
{
  unlink($tarfile) if (-e $tarfile);
  unlink($gzfile) if (-e $gzfile);
  print color("red"),"Archiving failed. Please manually compress and store the '$outputdir' directory.\n";
  print color("reset");
}

print "\nCompleted processing. Please copy the directory '$reportsdir' to a Windows machine. Open the macro.xls and click 'Generate report' to generate the final reports.\n";

sub extractTop()
{
  print "Extracting system top data\n";
  if(! -e "$outputdir/topoutput_0.txt" )
  {
    print color("yellow"),"top data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
  my $filename = "$outputdir/topoutput_0.txt";
  my @timestamp = `cat $filename | grep ^mystattop | awk '{print \$3}'`;
  my @Cpu = `cat $filename | grep Cpu`;
  my @Mem = `cat $filename | grep Mem:`;
  my @Swap = `cat $filename | grep Swap:`;
  
  #chomp(@timestamp);
  chomp(@Cpu);
  chomp(@Mem);
  chomp(@timestamp);
  chomp(@Swap);
  
  open (REPFILE, ">$reportsdir/finaltop.csv") or die "Creation of report file failed\n";
  print REPFILE "TIMESTAMP,Cpu\%us,Cpu\%sys,Cpu\%nice,Cpu\%idle,Cpu\%wa,Cpu\%hwint,Cpu\%softintt,Cpu\%steal,Mem(total),Mem(used),Mem(free),Mem(buffers),Mem(cache)\n";
  
  for(my $i=0;$i<scalar(@timestamp);$i++)
  {
    my $c = $Cpu[$i];
    my $m = $Mem[$i];
    my $t = $timestamp[$i];
    my $s = $Swap[$i];
    print REPFILE "$t,";
       
    $c =~ m/Cpu\(s\):\s+([\d\.]+)%us,\s+([\d\.]+)%sy,\s+([\d\.]+)%ni,\s+([\d\.]+)%id,\s+([\d\.]+)%wa,\s+([\d\.]+)%hi,\s+([\d\.]+)%si,\s+([\d\.]+)%st/;    
    print REPFILE "$1,$2,$3,$4,$5,$6,$7,$8";
    
    $m =~ m/Mem:\s+(\d+)k\s+total,\s+(\d+)k\s+used,\s+(\d+)k\s+free,\s+(\d+)k\s+buffers/;
    print REPFILE ",$1,$2,$3,$4";
    
    $s =~ m/Swap:\s+(\d+)k\s+total,\s+(\d+)k\s+used,\s+(\d+)k\s+free,\s+(\d+)k\s+cached/;
    print REPFILE ",$4\n";
  }
  close(REPFILE);
  print "Wrote report as $reportsdir/finaltop.csv\n";
}

sub extractDstat()
{
  print "Extracting dstat data\n";
  if(! -e "$outputdir/dstatoutput_0.txt" )
  {
    print color("yellow"),"dstat data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
  
  open (DSTATFILE, "$outputdir/dstatoutput_0.txt") or die "Opening dstatoutput_0.txt file failed\n";
  open (DSTATCSVFILE, ">$reportsdir/dstat_report.csv") or die "Opening dstat_report.csv failed\n";
  
  my $i = 0;
  print DSTATCSVFILE "Date,Time,usr-cpu,sys-cpu,idl-cpu,wai-cpu,hiq-cpu,siq-cpu,read,writ,net-recv,net-send,paging-in,paging-out,int,csw\n";
  while (<DSTATFILE>)
  {
    my $line = $_; 
    $line =~ s/\|/ /g;
    $line =~ s/k/000/g;
    $line =~ s/M/000000/g;
    $line =~ s/B//g;
    chop($line);
    if($i>1){
      my @words = split(" ", $line);
      print DSTATCSVFILE join(",",@words);
      print DSTATCSVFILE "\n";
    }
    $i++;
  }
  
  close DSTATCSVFILE;
  close DSTATFILE;
}

sub extractFgwTop()
{
  print "Extracting FGW top data\n";
  if(! -e "$outputdir/topoutput_0.txt" )
  {
    print color("yellow"),"top data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
 
  my @fgwprocesses = `cat $outputdir/topoutput*.txt | awk '{print \$1,\$2,\$3,\$4,\$5,\$6,\$7}' |grep fgw|awk '{print \$7}'|sort|uniq`;
  chomp(@fgwprocesses);
  
  push(@fgwprocesses,"integ_sccp_m3ua_instance_a");
  push(@fgwprocesses,"integ_sccp_m3ua_instance_b");
  push(@fgwprocesses,"/usr/local/strongSwan/libexec/ipsec/charon");
  push(@fgwprocesses,"ss7_ph_stack");
  push(@fgwprocesses,"/usr/local/strongSwan/libexec/ipsec/starter");
  push(@fgwprocesses,"ntpd");

#   my @fgwprocesses;
#   push(@fgwprocesses,"/opt/fgw_instance_running/bin/fgwcap");

  my %workbookcpu;
  my %workbookres;
  my %workbookpid;
  
  my @timestamp;
  push(@timestamp,"TIMESTAMP");
  push(@timestamp,`cat $outputdir/topoutput_0.txt | grep ^mystattop | awk '{print \$3}'`);
  chomp(@timestamp);
  
  my $sampleCount = scalar(@timestamp);
  
  addToTopHash(\%workbookcpu,@timestamp);
  addToTopHash(\%workbookres,@timestamp);
  addToTopHash(\%workbookpid,@timestamp);
  
  foreach(@fgwprocesses)
  {
    print "Analysing [$_]...                    \r";
    &gettoparray($_,"\$3","RES",\%workbookres,$sampleCount);
    &gettoparray($_,"\$5","%CPU",\%workbookcpu,$sampleCount);
    &gettoparray($_,"\$1","PID",\%workbookpid,$sampleCount);
  }

  
  open (REPFILE, ">$reportsdir/finalfgwcpu.csv") or die "Creation of report file failed\n";
  foreach(sort{$a <=> $b}(keys(%workbookcpu)))
  {
    print "Writing row [$_]...                 \r";
    print REPFILE $workbookcpu{$_} . "\n";
  }
  close(REPFILE);

  open (REPFILE, ">$reportsdir/finalfgwres.csv") or die "Creation of report file failed\n";
  foreach(sort{$a <=> $b}(keys(%workbookres)))
  {
    print REPFILE $workbookres{$_} . "\n";
  }
  close(REPFILE);
  
  open (REPFILE, ">$reportsdir/finalfgwpid.csv") or die "Creation of report file failed\n";
  foreach(sort{$a <=> $b}(keys(%workbookpid)))
  {
    print REPFILE $workbookpid{$_} . "\n";
  }
  close(REPFILE);

  print "Wrote report as $reportsdir/finalfgwcpu.csv, $reportsdir/finalfgwres.csv, $reportsdir/finalfgwpid.csv\n";
}
sub gettoparray()
{
  my $processname = shift;
  my $column = shift;
  my $colname = shift;
  my $workbookref = shift;
  my $sampleCount = shift;
  
  my @column;
  push(@column,basename($processname)."-$colname");
  my @output = `grep $processname $outputdir/topoutput*.txt |awk '{print $column}'`;
  chomp(@output);
  return if (scalar(@output) eq 0);
  
  my@temp;
  foreach(@output)
  {
    if(m/([\d\.]+)m/)
    {
      $_ = "=$1*1024";
    }
    elsif(m/([\d\.]+)g/)
    {
      $_ = "=$1*1024*1024";
    }
    push(@temp,$_);
  }
  
#   print " Size of process [$processname] ". scalar(@temp)."\n";
#   print join(",",@temp);
#   <STDIN>;
  
  push(@column,@temp);
  if(scalar(@column) < $sampleCount )
  {
    print color("yellow"),"Stuffing additional data for [$processname] [$colname] because of missing samples. Probable process restart(s).\r";
    print color("reset");
    
    my $delta = $sampleCount - scalar(@column);
    for(my $i=0;$i<$delta;$i++)
    {
      push(@column,"0");
    }
  }
  elsif(scalar(@column) > $sampleCount)
  {
    print color("red"),"Additional data samples detected & removed for [$processname] [$colname].\n";
    print color("reset");
    my $delta = scalar(@column) - $sampleCount;
    for(my $i=0;$i<$delta;$i++)
    {
      pop(@column);
    }
  }
  
  addToTopHash($workbookref,@column);
}
sub addToTopHash()
{
  my $workbookref = shift;
  my @column = @_;
  for(my $i=0;$i<scalar(@column);$i++)
  {
    my $val = $column[$i];
    if(exists($$workbookref{$i}))
    {
      $val = $$workbookref{$i} . ",$val";
    }
    $$workbookref{$i} = $val;
    #print "Setting [$i] as [$val]\n";
  } 
}

sub extractInterrupts()
{
  print "Extracting interrupt data\n";
  if(! -e "$outputdir/interrupts.txt" )
  {
    print color("yellow"),"interrupts data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
  my $filename = "$outputdir/interrupts.txt";
  my @names = `cat $filename | awk '{print \$19}'|awk -F, '{print \$1}'|sort|uniq|grep -v '^\$'`;
  chomp(@names);

  my @timestamp = `grep TIMESTAMP $filename | awk -F: '{print \$2}'`;
  chomp(@timestamp);
  
  open (REPFILE, ">$reportsdir/finalinterrupts.csv") or die "Creation of report file failed\n";
  
  print REPFILE "TIMESTAMP,";
  foreach(@names)
  {
    for(my $i=0;$i<16;$i++)
    {
      print REPFILE "$_-CPU$i,";
    }
  }
  print REPFILE "\n";
  
  my %datahash;
  foreach(@names)
  {
    my @valArr = `grep $_ $filename | awk '{print \$2","\$3","\$4","\$5","\$6","\$7","\$8","\$9","\$10","\$11","\$12","\$13","\$14","\$15","\$16","\$17}'`;
    chomp(@valArr);
    $datahash{$_} = \@valArr;
  }
  for(my $i=0;$i<scalar(@timestamp);$i++)
  {
    my $dataRow = $timestamp[$i].",";
    foreach(@names)
    {
      my $dataArr = $datahash{$_};
      $dataRow = $dataRow . $$dataArr[$i].",";
    }
    print REPFILE "$dataRow\n";
  }
  close(REPFILE);
  print "Wrote report as $reportsdir/finalinterrupts.csv\n";
}

sub extractMpstat()
{
  print "Extracting mpstat data\n";
  if(! -e "$outputdir/mpstat.txt" )
  {
    print color("yellow"),"mpstat data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
  my $filename = "$outputdir/mpstat.txt";
  my @header = `grep CPU $filename`;
  chomp(@header);
  my $headerline = $header[0];
  @header = split(" ",$headerline);
  $header[0] = "Time";
  
  my @mpstat;
  push(@mpstat,join(",",@header));
  push(@mpstat,`grep all $filename`);
  chomp(@mpstat);
  open (REPFILE, ">$reportsdir/finalmpstat.csv") or die "Creation of report file failed\n";
  foreach(@mpstat)
  {
    s/(\s+)/,/g;
    print REPFILE "$_\n";
  }
  close(REPFILE);
  print "Wrote report as $reportsdir/finalmpstat.csv\n";
}

sub extractNetstat()
{
  print "Extracting netstat data\n";
  if(! -e "$outputdir/netstat.txt" )
  {
    print color("yellow"),"netstat data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
  my $filename = "$outputdir/netstat.txt";
  my %bytehash;
  my %pkthash;
  my @names = `cat $filename| grep : | grep -v TIMESTAMP | awk -F: '{print \$1}'|awk '{print \$1}'|sort|uniq`;
  chomp(@names);
  
  foreach(@names)
  {
    my @dataArray1 = `grep $_: $filename | awk -F: '{print \$2}' | awk '{print \$1","\$9}'`;
    chomp(@dataArray1);
    $bytehash{$_} = \@dataArray1;

    my @dataArray2 = `grep $_: $filename | awk -F: '{print \$2}' | awk '{print \$2","\$10}'`;
    chomp(@dataArray2);
    $pkthash{$_} = \@dataArray2;

  }

  my @timestamp = `grep TIMESTAMP $filename | awk -F: '{print \$2}'`;
  chomp(@timestamp);
    
  open (REPFILE1, ">$reportsdir/finalnetbytes.csv") or die "Creation of report file failed\n";
  open (REPFILE2, ">$reportsdir/finalnetpackets.csv") or die "Creation of report file failed\n";
  
  print REPFILE1 "TIMESTAMP,";
  print REPFILE2 "TIMESTAMP,";
  foreach(@names)
  {
    print REPFILE1 "$_-RX-BYTES,$_-TX-BYTES,";
    print REPFILE2 "$_-RX-PACKETS,$_-TX-PACKETS,";
  }
  print REPFILE1 "\n";
  print REPFILE2 "\n";
  
  for(my $count=0; $count < scalar(@timestamp); $count++)
  {
    my $byteRow = $timestamp[$count];
    my $pktRow = $timestamp[$count];
    foreach(@names)
    {
      my $dataArray = $bytehash{$_};
      $byteRow = $byteRow. $$dataArray[$count]. ",";

      $dataArray = $pkthash{$_};
      $pktRow = $pktRow. $$dataArray[$count]. ",";
    }
  	print REPFILE1 "$byteRow\n";
  	print REPFILE2 "$pktRow\n";
  }
  close(REPFILE1);
  close(REPFILE2);
  print "Wrote report as $reportsdir/finalnetpackets.csv\n";
  print "Wrote report as $reportsdir/finalnetbytes.csv\n";
}

sub extractLsof()
{
  print "Extracting system lsof data\n";
  if(! -e "$outputdir/lsof.txt" )
  {
    print color("yellow"),"lsof data not found, skipping...\n";
    print color("reset");   
    return;                                  
  }
  my @tags=("CHR","DIR","FIFO","IPv4","IPv6","REG","sock","unix","unknown");
  my $tags = shift;
  my $header;
  for (my $i = 0; $i < scalar(@tags) ; $i++ )
  {
    if($header)
    {
      $header = "$header,".$tags[$i];
    }
    else
    {
      $header = $tags[$i];
    }
  }
  `echo TIMESTAMP,$header > $reportsdir/finallsof.csv`;

  my @entireData = `cat $outputdir/lsof.txt|awk '{print \$5}'|grep -v COMMAND|grep -v TYPE`;
  my @blocks;
  my @currentblock;
  foreach(@entireData)
  {
    if(m/TIMESTAMP/)
    {
      #print "found start of block\n";
      #start of block, so save the current block
      if(scalar(@currentblock)>0)#to handle the first block
      {
        my @tempblock;
        push(@tempblock,@currentblock);
        push(@blocks,\@tempblock);
        #print @currentblock;
        @currentblock = ();#empty the current block
      }
      else
      {
        #print "empty block\n";
      }
    }
    push(@currentblock,$_);
    #print "Current block for loop count " . scalar(@currentblock) . "\n";
  }
  my @tempblock;
  push(@tempblock,@currentblock);
  push(@blocks,\@tempblock);

  foreach(@blocks)
  {
    my @oneblock = @$_;
    my %onestat;
    #print @oneblock;
    
    my $datarow = shift(@oneblock);
    chomp($datarow);
    $datarow =~ m/TIMESTAMP\:(.*)/;
    $datarow = $1;
    foreach(@oneblock)
    {
      chomp($_);
      my $currCount = 1;
      if(exists($onestat{$_}))
      {
        $currCount =  $onestat{$_} + 1;
      }
      $onestat{$_} = $currCount;
    }
    #print values(%onestat);
    #<STDIN>;
    foreach(@tags)
    {
      my $val = "";
      if(exists($onestat{$_}))
      {
        $val = $onestat{$_};
      }
      else
      {
        $val = "0";
      }
      if($datarow)
      {
        $datarow = "$datarow,$val";
      }
      else
      {
        $datarow = $val;
      }
    }
    #print "$datarow\n" ;
    `echo $datarow >> $reportsdir/finallsof.csv`;
  }
  print "Wrote report as $reportsdir/finallsof.csv\n";
}

sub extractSctp()
{
  my $filename = "$outputdir/sctp.txt";
  my $dataname = "sctp";
  my @keywords = ("SctpCurrEstab","SctpPassiveEstabs","SctpAborteds","SctpShutdowns","SctpOutCtrlChunks","SctpOutOrderChunks","SctpInCtrlChunks","SctpInOrderChunks","SctpOutSCTPPacks","SctpInSCTPPacks","SctpDelaySackExpireds","SctpInPktSoftirq","SctpInPktBacklog");
   
  print "Extracting $dataname data\n";
  if(! -e $filename )
  {
    print color("yellow"),"sctp data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
  my @timestamp = `cat $filename | grep TIMESTAMP | awk -F: '{print \$2}'`;
  chomp(@timestamp);
  
  my %data;
  for(my $i=0;$i < scalar(@timestamp); $i++)
  {
    $data{$i} = $timestamp[$i];
  }
  
  foreach(@keywords)
  {
    print "Analysing [$_]...                    \r";
    my @datacolumn = `cat $filename | grep $_ | awk '{print \$2}'`;
    chomp(@datacolumn);
    
    for(my $i=0;$i < scalar(@datacolumn); $i++)
    {
      my $val = $data{$i};
      $val = "$val,".$datacolumn[$i];
      $data{$i} = $val;
    }
  }
  open (REPFILE, ">$reportsdir/finalsctp.csv") or die "Creation of report file failed\n";
  print REPFILE "TIMESTAMP,SctpCurrEstab,SctpPassiveEstabs,SctpAborteds,SctpShutdowns,SctpOutCtrlChunks,SctpOutOrderChunks,SctpInCtrlChunks,SctpInOrderChunks,SctpOutSCTPPacks,SctpInSCTPPacks,SctpDelaySackExpireds,SctpInPktSoftirq,SctpInPktBacklog\n";
  
  foreach(sort{$a <=> $b}(keys(%data)))
  {
    print REPFILE $data{$_} . "\n";
  }
  close(REPFILE);
  print "Wrote report as $reportsdir/finalsctp.csv\n";
}

sub extractIpsec()
{
  print "Extracting ipsec tunnel data\n";
  my $filename = "$outputdir/ipsecstat.txt";
  if(! -e $filename )
  {
    print color("yellow"),"ipsec data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
  copy("$outputdir/ipsecstat.txt", "$reportsdir/finalipsec.csv");
  print "Wrote report as $reportsdir/finalipsec.csv\n";
}

sub extractMsgq()
{
  print "Extracting msgq data\n";
  my $filename = "$outputdir/msgq.txt";
  if(! -e $filename )
  {
    print color("yellow"),"message queue data not found, skipping...\n";
    print color("reset");
    return;                                     
  }
  open(INP, "<$outputdir/msgqlist.txt");
  my(@mqgqfiles) = <INP>;
  chomp(@mqgqfiles);
  
  my $header = join(",",@mqgqfiles);

  my @timestamp = `cat $filename | grep TIMESTAMP | awk -F: '{print \$2}'`;
  chomp(@timestamp);
  
  my %data;
  for(my $i=0;$i < scalar(@timestamp); $i++)
  {
    $data{$i} = $timestamp[$i];
  }
    
  foreach(@mqgqfiles)
  {
    print "Analysing [$_]...                    \r";
    my @datacolumn = `cat $filename | grep "$_ " | awk '{print \$2}'|awk -F: '{print \$2}'`;
    chomp(@datacolumn);    
    for(my $i=0;$i < scalar(@datacolumn); $i++)
    {
      my $val = $data{$i};
      $val = "$val,".$datacolumn[$i];
      $data{$i} = $val;
    }
  }
  open (REPFILE, ">$reportsdir/finalmsgq.csv") or die "Creation of report file failed\n";
  print REPFILE "TIMESTAMP,$header\n";
  
  foreach(sort{$a <=> $b}(keys(%data)))
  {
    print REPFILE $data{$_} . "\n";
  }
  close(REPFILE);
  print "Wrote report as $reportsdir/finalmsgq.csv\n";

  `cat $outputdir/msgqmap.txt | awk '{print \$1","\$3}' > $reportsdir/finalmsgqmap.csv` if (-e "$outputdir/msgqmap.txt");
}

sub extractLisaEventViewer()
{
  print "Formatting LisaEventViewer data ...\n";
  if(! -e "$outputdir/EventViewerOutput.out" )
  {
    print color("yellow"),"LisaEventViewer data not found, skipping...\n";
    print color("reset");   
    return;                                  
  }
  open (EVENTVIEWEROUTFILE, "$outputdir/EventViewerOutput.out");
  open (EVENTVIEWERCSVFILE, ">reports/EventViewerOutput.csv") or die "Creation of EventViewerOutput.csv failed!\n";
  
  my $linecount = 0;
  while(<EVENTVIEWEROUTFILE>)
  {
    my $line = $_;
    my @words = split(" ", $line);
    if ($linecount < 5){
      $line =~ s/,/ /;
      print EVENTVIEWERCSVFILE $line;
    }
    if ($linecount == 5){
      print EVENTVIEWERCSVFILE join(",",@words);
      print EVENTVIEWERCSVFILE "\n";
    }
    if ($linecount > 5)
    {
      my $i=0;
      my $lisaeventcsvline = "";
      for ($i=0; $i<7; $i++){$lisaeventcsvline = $lisaeventcsvline . "$words[$i]," }
      my $timestamp = join " ", $words[7],$words[8],$words[9],$words[10],$words[11];
      $lisaeventcsvline = $lisaeventcsvline . $timestamp . ",";
      my $word;
      $i=0;
      foreach $word (@words){ 
        if ($i > 11){
          $word =~ s/,/ /;
          $lisaeventcsvline = $lisaeventcsvline . " $word";
        }
        $i++;
      }
      print EVENTVIEWERCSVFILE "$lisaeventcsvline\n";
    }
    $linecount++;
  }
  
  close (EVENTVIEWERCSVFILE);
  close (EVENTVIEWEROUTFILE);
}

sub gettime()
{
	my ($sec,$min,$hour,$day,$month,$yr19,@rest) =   localtime(time);
	return "$day-".++$month."-".($yr19+1900)."_".sprintf("%02d",$hour)."_".sprintf("%02d",$min)."_".sprintf("%02d",$sec);
}

sub archiveOldFiles
{
  my $date = &gettime();
  my $tarfile = "$date.reports.tar";
  my $gzfile = "$date.reports.tar.gz";
  if(-e $reportsdir)
  {
    print "Archiving existing '$reportsdir' directory to $gzfile                      \r";
    `tar -cvf $tarfile $reportsdir/ > /dev/null 2>&1`;
    `gzip $tarfile > /dev/null 2>&1` if !$?;
    `tar tvfz $gzfile > /dev/null 2>&1`;
    if ( !$? )
    {
      `rm -rf $reportsdir > /dev/null 2>&1`; 
      print "Archived existing '$reportsdir' directory to $gzfile              \n";
    }
    else
    {
      unlink("$tarfile") if (-e "$tarfile");
      unlink("$gzfile") if (-e "$gzfile");
      print "Moved existing '$reportsdir' directory to $date.$reportsdir                     \n";
      `mv $reportsdir $date.$reportsdir > /dev/null 2>&1`;
    }
  }
}

sub printhelp
{  
  print STDERR <<EOF;
  usage: $0 [h] [-r reportsdir] [-o sourcedir]
     -h                  : this (help) message
     -r reportsdir       : directory in which the reports to be generated
     -o sourcedir        : output directory from which to analyse the statistics
EOF
  exit(0);
}
#ike parsing from /var/log/messages starts
sub ike_message_parser
{
        my $pathoflogfile= shift;#passing path of log message as argument while running
        print $pathoflogfile;
        my $parsedikeinit = `grep "parsed IKE_SA_INIT" $pathoflogfile |cut -d ' ' -f 1-3`;
        my $retransmitIKE_SA_INIT = `grep "received retransmit of request with ID 0" $pathoflogfile |cut -d ' ' -f 1-3`;
        my $retransmitIKE_AUTH = `grep "received retransmit of request with ID 1" $pathoflogfile |cut -d ' ' -f 1-3`;
        my $parsed_IKE_SA_INIT_File ="$reportsdir/Parsed_IKE_INIT_File.csv";
        my $retransmitIKE_SA_INIT_File="$reportsdir/Retransmit_IKE_INIT_File.csv";
        my $retransmitIKE_AUTH_File="$reportsdir/Retransmit_IKE_AUTH_File.csv";
        &countFrequency($parsedikeinit,$parsed_IKE_SA_INIT_File);
        &countFrequency($retransmitIKE_SA_INIT,$retransmitIKE_SA_INIT_File);
        &countFrequency($retransmitIKE_AUTH,$retransmitIKE_AUTH_File);
}

sub countFrequency
{
    my ($returncode,$filename) = @_;
    my $word;
    my %freq;
    foreach $word (split('\n', $returncode)) {
        $freq{$word}++;
    }
open (MYFILE, ">$filename") or die "\nERROR: Cannot create/open $filename\n";
my @filevalue = split('_', $filename);
print MYFILE "MM DD TIME,Rate of $filevalue[0]$filevalue[1]$filevalue[2] Per Second\n";
foreach $word (sort keys %freq) {

        my @values = split(' ', $word);
        split(' ',$word);
    print MYFILE "@values[0] @values[1] @values[2],$freq{$word}\n";
}

close (MYFILE);
}

#ike parsing from /var/log/messages ends


